from pygimli.physics import ert

res = ert.ERTManager('gallery.dat')
res.invert()
res.showResultAndFit()
