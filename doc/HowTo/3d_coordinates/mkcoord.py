# -*- coding: utf-8 -*-
"""
Created on Mon May 30 15:13:41 2011
modified: 
    Carsten 04.01.12 add pybert dependency
    
@author: Guenther.T
"""

import pygimli as pg
import pybert as pb
import matplotlib.pyplot as plt
import numpy as np
from pygimli.curvefit import harmfit

doplot = False
#A = pg.RMatrix( )
#pg.loadMatrixCol( A, 'gps.txt' )
#tt, hh, yy, xx = A

tt, hh, yy, xx = np.loadtxt('gps.txt', unpack=True)
x = harmfit( xx, tt, nCoefficients = 10 )[ 0 ]
y = harmfit( yy, tt, nCoefficients = 10 )[ 0 ]
z = harmfit( hh, tt, nCoefficients = 15 )[ 0 ]

if doplot:
    fig, ax = plt.figure(ncols=2)

    ax[0].plot(tt, hh, 'bx-', tt, z, 'r-')
    ax[0].grid(True, which='both')
    ax[0].set_xlabel('profile distance in m')
    ax[0].set_ylabel('height in m')

    ax[1].plot(xx, yy, 'bx-', x, y, 'r-')
    ax[1].grid(True, which='both')
    ax[1].set_xlabel('x in m')
    ax[1].set_ylabel('y in m')
    plt.show()

data = pb.DataContainerERT('profile.dat')
xe = [pos[0] for pos in data.sensorPositions()]
t = np.hstack((0., np.cumsum(np.sqrt(np.diff(x)**2 + np.diff(y)**2))))

x2d = np.interp(xe, tt, t)
z2d = np.interp(xe, tt, z)

POS = np.vstack((x2d, x, y))
np.savetxt('pos.map', POS.T)

for i in range(data.sensorCount()):
    pp = pg.RVector3(x2d[i], 0.0, z2d[i])
    data.setSensorPosition(i, pp)

data.save('profile2d.ohm', 'a b m n u i', 'x z')
