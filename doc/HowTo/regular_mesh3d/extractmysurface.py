# -*- coding: utf-8 -*-
"""
Created on Wed Jan 19 11:54:19 2011

@author: Guenther.T
"""

import pygimli as g

worldBoundary = g.Mesh('worldBoundary.bms')
worldPoly = g.Mesh()
worldPoly.createMeshByBoundaries(worldBoundary,
                                 worldBoundary.findBoundaryByMarker(-2, 0))
worldPoly.exportAsTetgenPolyFile("worldBoundary.poly")
