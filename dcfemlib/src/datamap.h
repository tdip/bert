// Copyright (C) 2005 Carsten R�cker <cruecker@uni-leipzig.de>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//

#ifndef DATAMAP__H
#define DATAMAP__H

#include "dcfemlib.h"
using namespace DCFEMLib;

#include "elements.h"
#include "basemesh.h"
using namespace MyMesh;

#include "myvec/vector.h"

#include <vector>
using namespace std;


/*! Returns a double value standardised homogeneous potential U for the
 destination RealPos destPos with an sourcelocation sourcePos. Resistifity R and current I is 1.0.
(U = R * I * K) where K depend on \ref SPACECONFIG config: \n
config = HALFSPACE: K = 1/( 2 PI r), where r is the distance between each node and sourcePos\n
config = FULLSPACE: K = 1/( 4 PI r)\n
config = MIRRORSOURCE: K = 1/( 4 PI r) + 1/(4 PI r_m), where r_m is the distance between each node and sourcePos mirrored at mirrorPlaneZ \n
if config = MIRRORSOURCE is given, mirrorPlaneZ must be defined ( default = 0.0 ).\n
Note: if the sourcePos the same like the destPos the potential is undefined and forced to 0.0*/
double standardisedPotential( const RealPos & sourcePos, const RealPos & destPos,
				SpaceConfigEnum config = MIRRORSOURCE,
				double mirrorPlaneZ = 0.0);

/*! Returns a  double value of standardised homogeneous potential U for the Dipol-Dipol-configuration Pos a, b, m, n. \n
See for details: \ref standardisedPotential( const RealPos & sourcePos, const RealPos & destPos, SPACECONFIG config = HALFSPACE,
				      double mirrorPlaneZ = 0.0 );
*/
double standardisedPotential( const RealPos & a, const RealPos & b, const RealPos & m, const RealPos & n,
			      SpaceConfigEnum config = MIRRORSOURCE, double mirrorPlaneZ = 0.0 );

/*! Returns a RVector ( MyVec::vector< double> ) of standardised homogeneous potential U for each \ref Node in the
  \ref vector nodeVec with an sourcelocation sourcePos. \n
See for details: \ref standardisedPotential( const RealPos & sourcePos, const RealPos & destPos, SPACECONFIG config = HALFSPACE,
				      double mirrorPlaneZ = 0.0 );
*/
RVector standardisedPotential( const RealPos & sourcePos, const vector< Node *> & nodeVec,
			       SpaceConfigEnum config = MIRRORSOURCE,
			       double mirrorPlaneZ = 0.0 );
/*! Returns a RVector ( MyVec::vector< double> ) of standardised homogeneous potential U for each \ref Node in the
  \ref vector nodeVec with an sourcelocation sourcePos. \n
See for details: \ref standardisedPotential( const RealPos & sourcePos, const RealPos & destPos, SPACECONFIG config = HALFSPACE,
				      double mirrorPlaneZ = 0.0 );
*/
RVector standardisedPotential( const RealPos & sourcePos, const vector< Node > & nodeVec,
			       SpaceConfigEnum config = MIRRORSOURCE,
			       double mirrorPlaneZ = 0.0 );

/*! Returns a RVector ( MyVec::vector< double> ) of standardised homogeneous potential U for each \ref Node in the
  \ref BaseMesh mesh with an sourcelocation sourcePos. \n
See for details: \ref standardisedPotential( const RealPos & sourcePos, vector< Node *> nodeVec, SPACECONFIG config = HALFSPACE,
				      double mirrorPlaneZ = 0.0 );
*/
RVector standardisedPotential( const RealPos & sourcePos, const BaseMesh & mesh,
			       SpaceConfigEnum config = MIRRORSOURCE,
			       double mirrorPlaneZ = 0.0 );

class ElectrodeConfig;
class ElectrodeConfigVector;
class DLLEXPORT DataMap;

double standardisedPotential( const ElectrodeConfig & econfig, const DataMap & datamap,
			       SpaceConfigEnum config = MIRRORSOURCE,
			       double mirrorPlaneZ = 0.0 );


double standardisedPotential( const ElectrodeConfigVector & profile, int i,
			       SpaceConfigEnum config = MIRRORSOURCE,
			       double mirrorPlaneZ = 0.0 );



DLLEXPORT ElectrodeConfigVector createPolPolSounding( int A, int first, int last );
DLLEXPORT ElectrodeConfigVector createDipolPolSounding( int A, int B, int first, int last );
DLLEXPORT ElectrodeConfigVector createDipolDipolSounding( int A, int B, int first, int last );

DLLEXPORT ElectrodeConfigVector createWennerSection( int electrodeCount );
DLLEXPORT ElectrodeConfigVector createPolPolSection( int first, int last );
DLLEXPORT ElectrodeConfigVector createDipolDipolSection( int first, int last, int step, bool circle );

//!A geoelectrical data container.
class DLLEXPORT DataMap : public vector< RVector >{
public:
  DataMap( ){
    mesh_ = NULL;
    setDefaults_();
  }
  DataMap( BaseMesh & mesh, const vector < RealPos > & vecSourcePos, bool sort = false )
    : mesh_( & mesh ), vecSourcePos_( vecSourcePos ){
    setDefaults_();
    allocateSpace_( vecSourcePos_.size() );
  }
  DataMap( BaseMesh & mesh, bool sort = false )
    : mesh_( & mesh ){
    findElectrodePosition( *mesh_, sort );
    vecSourcePos_ = mesh_->positions( mesh_->getAllMarkedNodes( -99 ) );
    setDefaults_();
    allocateSpace_( vecSourcePos_.size() );
  }
//   DataMap( const vector < RealPos > & sources, bool sort = false )
//     :vecSourcePos_( sources) {
//     setDefaults_();
//   }
  DataMap( const string & filename ){
    setDefaults_();
    load( filename );
  }

//   DataMap( const DataMap & map  );

//   DataMap & operator = ( const DataMap & map  );

//   DataMap & operator -= ( const DataMap & map  );

  virtual ~DataMap(){ clearAll(); }

  int clearAll(){
    sourceNodes_.clear();
    this->clear();
    return 1;
  }

  int findElectrodePosition( const BaseMesh & mesh, bool sort = false );

  int filled( int i ){
    if ( fabs( (*this)[ i ][ 0 ] ) < 1E-40 && fabs( (*this)[ i ][ 1 ] ) < 1E-40 ) return 0; else return 1;
  }
  RVector data( const ElectrodeConfigVector & profil, bool reciprocity = false ) const ;

  int load( const string & filename );

  int save( const string & filename );

  int saveSelectedData( const string & outfilename, const ElectrodeConfigVector & profil,
			bool fromOne,
			SpaceConfigEnum spaceConfig = HALFSPACE, double mirrorPlaneZ = 0.0);

  int saveConfigList( const string & outfilename, const string & infilename,
		      SpaceConfigEnum config = HALFSPACE, double mirrorPlaneZ = 0.0 );

  int saveDipolDipolData( const string & filename,
			  int step = 1, int first = 0, int last = -1,
			  SpaceConfigEnum config = HALFSPACE );

  int savePolPolData( const string & filename,
		      int first = 0, int last = -1,
		      SpaceConfigEnum config = HALFSPACE );


  void setDipoleSources( bool dipole ) { dipole_ = dipole; }
  bool dipoleSources(  ){ return dipole_; }

  void setCircleGeometry( bool circle ) { circle_ = circle; if ( circle ) dipole_ = true; }
  bool circleGeometry(  ){ return circle_; }

  void importSolution( const string & fileBody );
  void importSolution( int position, const RVector & solution );
  void importSolution( const vector < RVector > & solution );
  double findNearestSolution( int posIdx, const RVector & solution );

  void collectData( const vector < RVector > & data );

  void setElectrodePositions( const vector < Node > & sources ){
   sourceNodes_ = sources;
    for ( size_t i = 0, imax =  sourceNodes_.size(); i < imax; i ++ ) {
      vecSourcePos_.push_back( sourceNodes_[ i ].pos() );
    }
    allocateSpace_( sourceNodes_.size() );
  }
  vector < Node > electrodeNodes( ){ return sourceNodes_; }
  vector < RealPos > electrodePositions( ){ return vecSourcePos_; }

  RealPos electrodePosition( int i ) const { return sourceNodes_[ i ].pos(); }

  /*!Returns a \ref RVector of potential values for each nodes. The integer value source gives the position index for the current injection point. */
  inline RVector potential( int source ){;
    return (*this)[ source ];
  }
  /*!Returns a \ref RVector of distances between all nodes and the node which is gives by the integer position index source.*/
  RVector vectorOfDistancesFrom( int source );

  //  void createSoundingFromProfile( const ElectrodeConfigVector & profile );

  int saveDipolDipol( const string & filename,
		      int step = 1, int first = 0, int last = -1,
		      SpaceConfigEnum config = HALFSPACE );
  int saveDipolDipolKoord( const string & filename, int step = 1, int first = 0, int last = -1 );
  int savePolPolKoord( const string & filename, int step = 1, int first = 0, int last = -1 );
  int saveDipolDipolSounding( const string & filename, int nodeA = 0, int nodeB = 1,
			      SpaceConfigEnum config = HALFSPACE );
  int savePolPolSounding( const string & filename, int nodeA = 0, int step = 1, int first = 0, int last = -1,
			  SpaceConfigEnum config = HALFSPACE );

  bool verbose ( ) const { return verbose_; }
  void setVerbose ( bool verbose ) { verbose_ = verbose; }

protected:
  void setDefaults_(){ circle_ = false; dipole_ = false; verbose_ = false; }

  void allocateSpace_( int size ){
    this->clear();
    for ( int i = 0; i < size; i ++ ) {
      this->push_back( RVector( size, 0.0 ) );
    }
  }

  BaseMesh * mesh_;
  vector < Node > sourceNodes_;
  vector < RealPos > vecSourcePos_;
  bool dipole_;
  bool circle_;
  bool verbose_;
};


// int saveDataMap( const string & filename, vector < RVector * > & dataMap );
// int loadDataMap( const string & filename, vector < RVector * > & dataMap );

// void savePolPolMap( RVector & pot, Mesh2D & mesh, const string & filename,
// 		    map<double, int> & nodeMap, double current, int s1);

// void savePotentialMap( RVector & x, Mesh2D & mesh, const string & filename,
// 		       map<double, int> & map, char koord = 'x' );

// void savePotentialVector( RVector & x, const string & filename, vector<int> & vec);

// void saveDipolPol( vector< RVector > & xlist, Mesh2D & mesh, const string & filename,
// 		   map<double, int> & map, double current, int S1, int S2);


// void savePolPolVector( RVector & pot, Mesh2D & mesh, const string & filename,
// 		       vector<int> & vec, double current, int s1);
#endif // DATAMAP__H

/*
$Log: datamap.h,v $
Revision 1.17  2010/06/22 14:42:49  thomas
*** empty log message ***

Revision 1.16  2010/06/18 10:17:59  carsten
win32 compatibility commit

Revision 1.15  2007/12/13 18:42:16  carsten
*** empty log message ***

Revision 1.14  2007/10/04 07:21:59  thomas
BERT + polytools codeblocks (dllexport)

Revision 1.13  2007/03/07 16:38:14  carsten
*** empty log message ***

Revision 1.12  2005/07/04 15:11:22  carsten
*** empty log message ***

Revision 1.11  2005/06/27 09:28:55  carsten
*** empty log message ***

Revision 1.10  2005/06/23 20:15:32  carsten
*** empty log message ***

Revision 1.9  2005/04/08 14:09:26  carsten
*** empty log message ***

Revision 1.8  2005/03/29 16:57:46  carsten
add inversion tools

Revision 1.7  2005/03/08 18:20:35  carsten
*** empty log message ***

Revision 1.6  2005/03/08 16:04:45  carsten
*** empty log message ***

Revision 1.5  2005/02/07 13:33:13  carsten
*** empty log message ***

Revision 1.4  2005/01/13 21:12:15  carsten
*** empty log message ***

Revision 1.3  2005/01/13 20:10:42  carsten
*** empty log message ***

Revision 1.2  2005/01/12 21:06:45  carsten
*** empty log message ***

Revision 1.1  2005/01/04 19:22:30  carsten
add in cvs

*/
