// Copyright (C) 2005 Carsten Rücker <cruecker@uni-leipzig.de>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//

#include "basemesh.h"
#include "mesh2d.h"
#include "mesh3d.h"

#include <cstring>
#include <string>
#include <set>

#include <cassert>
using namespace std;
using namespace MyVec;


namespace MyMesh{

#include "setalgorithm.h"

void findMinMaxVals( const BaseMesh & mesh, double & xmin, double & xmax, double & ymin,
		     double & ymax, double & zmin, double & zmax, bool reset ){
  if ( mesh.nodeCount() > 0 ){
    if ( reset ){
      xmin = 9e99, xmax = -9e99; ymin = 9e99, ymax = -9e99; zmin = 9e99, zmax = -9e99;
    }

    for ( int i = 0; i < mesh.nodeCount(); i++  ){
      xmin = min( xmin, mesh.node( i ).x() ); xmax = max( xmax, mesh.node( i ).x() );
      ymin = min( ymin, mesh.node( i ).y() ); ymax = max( ymax, mesh.node( i ).y() );
      zmin = min( zmin, mesh.node( i ).z() ); zmax = max( zmax, mesh.node( i ).z() );
    }
  }
}

void findMinMaxVals( const vector < RealPos >  & posVector,  double & xmin, double & xmax,
		     double & ymin, double & ymax, double & zmin, double & zmax, bool reset ){
  if ( posVector.size() > 0 ){
    if ( reset ){
      xmin = 9e99, xmax = -9e99; ymin = 9e99, ymax = -9e99; zmin = 9e99, zmax = -9e99;
    }

    for ( size_t i = 0; i < posVector.size(); i++  ){
      xmin = min( xmin, posVector[ i ].x() ); xmax = max( xmax, posVector[ i ].x() );
      ymin = min( ymin, posVector[ i ].y() ); ymax = max( ymax, posVector[ i ].y() );
      zmin = min( zmin, posVector[ i ].z() ); zmax = max( zmax, posVector[ i ].z() );
    }
  }
}

vector < Node * > commonNodes( const BaseElement & e0, const BaseElement & e1 ){
  vector < Node * > nodes;

  SetpNodes n0; for ( int i = 0; i < e0.nodeCount(); i ++ ) n0.insert( & e0.node(i) );
  SetpNodes n1; for ( int i = 0; i < e1.nodeCount(); i ++ ) n1.insert( & e1.node(i) );


  for ( SetpNodes::const_iterator it = n0.begin(); it != n0.end(); it++ ){
    if ( n1.find( (*it) ) != n1.end() ){
      nodes.push_back( (*it) );
    }
  }
  return nodes;
}

double area( const vector < Node * > & nodes ){
  switch( nodes.size() ){
  case 2:
    return Edge( *nodes[ 0 ], *nodes[ 1 ] ).length();
    break;
  case 3:
    return TriangleFace( *nodes[ 0 ], *nodes[ 1 ], *nodes[ 2 ] ).area() ;
    break;
  default:
    cerr << WHERE_AM_I << " to implement " << nodes.size() << endl;
    return 0.0;
  }
}

BaseMesh::BaseMesh( ){
  _isworldmatrixknown = 0;
  xmin_ = 0; xmax_ = 0; ymin_ = 0; ymax_ = 0; zmin_ = 0; zmax_ = 0;
}

BaseMesh::BaseMesh( const BaseMesh & mesh ) {
  _isworldmatrixknown = 0;
  clear();

  for ( int i = 0; i < mesh.nodeCount(); i++) createNode( mesh.node(i) );
  for ( int i = 0; i < mesh.markerCount(); i++) createMarker( mesh.marker(i) );
}

BaseMesh::BaseMesh( const string & fname ) {
  load( fname );
}

BaseMesh & BaseMesh::operator = ( const BaseMesh & mesh ){
  //cout << WHERE_AM_I << this << " = " << &mesh << endl;
//   _refined = mesh.refinedVector();
  _isworldmatrixknown = 0;
  if ( this != & mesh ){
    clear();
    for ( int i = 0; i < mesh.nodeCount(); i++) createNode( mesh.node( i ) );
    for ( int i = 0; i < mesh.cellCount(); i++) createCell( mesh.cell( i ) );
    for ( int i = 0; i < mesh.boundaryCount(); i++) createBoundary( mesh.boundary( i ) );
    for ( int i = 0; i < mesh.markerCount(); i++) createMarker( mesh.marker( i ) );
  }
  return * this;
}

BaseMesh::~BaseMesh(){
  //  cout << WHERE_AM_I << this << endl;
  clear();
}

void BaseMesh::clear(){
//  cout << "BaseMesh::clear" << this << endl;

 //  deleteAllNodes();

  //  cout << "this->nodeCount() " << this->nodeCount()<< endl;
  if ( pNodeVector_.size() > 0 ){
    for ( int i = 0, imax = pNodeVector_.size(); i < imax; i ++){
      if ( pNodeVector_[ i ] ) {
            delete pNodeVector_[ i ];
            pNodeVector_[ i ] = NULL;
        }
    }
  }
  //  cout << "this->edgeCount() " << this->edgeCount()<< endl;
    if ( pBoundaryVector_.size() > 0 ){
        for ( int i = 0, imax = pBoundaryVector_.size(); i < imax; i ++){
            if ( pBoundaryVector_[ i ] ) delete pBoundaryVector_[ i ];
        }
    }

    //   cout << "this->cellCount() " << this->cellCount()<< endl;
//   cout << "this->cellCount() " << pCellVector_.size() << endl;
    deleteAllCells();

  //  cout << "this->regionCount() " << regions_.size()<< endl;
    for ( size_t i = 0, imax = regions_.size(); i < imax; i ++){
        if ( regions_[ i ] ) delete regions_[ i ];
    }

    _isworldmatrixknown = 0;

    regions_.clear();
    pNodeVector_.clear();
    pBoundaryVector_.clear();
}

void BaseMesh::deleteAllCells(){
  if ( pCellVector_.size() > 0 ) {
    for ( int i = 0, imax = pCellVector_.size(); i < imax; i ++){
      if ( pCellVector_[ i ] )  delete pCellVector_[ i ];
    }
  }
  pCellVector_.clear();
}

Node * BaseMesh::createNode_( double x, double y, double z, int id, int marker){
  if ( id == -1 ) id = nodeCount();
  Node * n = new Node( x, y, z, id, marker);
  pNodeVector_.push_back( n );
  return n;
}

BaseElement * BaseMesh::createCell( const BaseElement & cell ){
  vector < long > idx;
  for ( int i = 0; i < cell.nodeCount(); i ++ ) idx.push_back( cell.node( i ).id() );

  return createCellFromNodeIdxVector( idx, cell.attribute() );
}

BaseElement * BaseMesh::createBoundary( const BaseElement & bound ){
  vector < long > idx;
  for ( int i = 0; i < bound.nodeCount(); i ++ ) idx.push_back( bound.node( i ).id() );
  return createBoundaryFromNodeIdxVector( idx, bound.marker() );
}

// Edge * BaseMesh::createEdge( Edge & a ){
//   Edge * e = createEdge( * pNodeVector_[ a.nodeA().id() ],
// 			 * pNodeVector_[ a.nodeB().id() ],
// 			 a.id(), a.marker() );
//   return e;
// }

// Edge * BaseMesh::createEdge( Node & a, Node & b, int id, int marker ){
//   if ( id == -1 ) id = pBoundaryVector_.size();
//   Edge * e = new Edge( a, b, id, marker );
//   //  cout << "create Edge " << id << " node a: " <<a.id() << "\t" <<b.id() << endl;
//   pBoundaryVector_.push_back( e );
//   return e;
// }

// Edge3 * BaseMesh::createEdge3( Node & a, Node & b, Node & c, int id, int marker ){
//   if ( id == -1 ) id = pBoundaryVector_.size();

//   Edge3 * e = new Edge3( a, b, c, id, marker );

//   pBoundaryVector_.push_back( e );
//   return e;
// }

RegionMarker * BaseMesh::createMarker( const RegionMarker & m ){
  return createRegionMarker( m.pos(), m.attribute(), m.dx() );
}

RegionMarker * BaseMesh::createRegionMarker( double rx, double rz, double attribute, double dx){
  RegionMarker * m = new RegionMarker(rx, rz, attribute, dx);
  regions_.push_back( m );
  return m;
}

RegionMarker * BaseMesh::createRegionMarker( const RealPos & pos, double attribute, double dx){
  RegionMarker * m = new RegionMarker( pos, attribute, dx);
  regions_.push_back( m );
  return m;
}

void BaseMesh::deleteAllNodes(){
 //  while( pNodeVector_.begin() != pNodeVector_.end() ) deleteNode( * pNodeVector_.front() );
//   Node * node;
//   for (int i = 0, imax = pNodeVector_.size(); i < imax; i++){
//     node = pNodeVector_[ i ];
//   }
//   SetpEdges tmp; tmp = merge( node->toSet(), node->fromSet() );
//   for ( SetpEdges::iterator it = tmp.begin(); it != tmp.end(); it++) eraseEdge( *(*it) );
//   delete node;
//   pNodeVector_.clear();
}

void BaseMesh::deleteNode( Node & node){;
//  for (VectorpNodes::iterator it = pNodeVector_.begin(); it != pNodeVector_.end(); it ++ ){
//    if ( (*it) == & node ){
//      pNodeVector_.erase( (it) );
//      break;
//    }
//  }

//  SetpEdges tmp; tmp = merge( node.toSet(), node.fromSet() );
//  for ( SetpEdges::iterator it = tmp.begin(); it != tmp.end(); it++) eraseEdge( *(*it) );

//  delete & node;
}

void BaseMesh::deleteEdge( Edge & edge ){
  //  cout << "Delete edge " << edge << endl;
  eraseEdge( edge );
  delete & edge;
}

void BaseMesh::eraseEdge( Edge & edge ){
  for ( VectorpElements::iterator it = pBoundaryVector_.begin(); it != pBoundaryVector_.end(); it ++ ){
    if ( (*it) == dynamic_cast< Edge * >(& edge) ){
      if ( edge.leftCell() != NULL ) eraseElement( * edge.leftCell() );
      if ( edge.rightCell() != NULL ) eraseElement( * edge.rightCell() );
      pBoundaryVector_.erase( (it) );
      break;
    }
  }
}

void BaseMesh::eraseElement( BaseElement & element ){
  for ( VectorpElements::iterator it = pCellVector_.begin(); it != pCellVector_.end(); it ++ ){
    if ( (*it) == & element ){
      pCellVector_.erase( (it) );
      break;
    }
  }
}

Node & BaseMesh::node(int i) const {
  //cout << i << endl;
  assert( i > -2 && i < (int) pNodeVector_.size() );
  if ( i == -1 ) return (*new Node());
  assert( pNodeVector_[i] );
  return (* pNodeVector_[i]);
}
// BaseElement & BaseMesh::edge( int i ) const {
//   assert(i > -1 && i < (int) pBoundaryVector_.size());
//   return (* pBoundaryVector_[i]);
// }

BaseElement & BaseMesh::element(int i) const {
  //  cout << i << " " << pCellVector_.size() << endl;
  assert( i > -1 && i < (int)pCellVector_.size() );

  return (* pCellVector_[i]);
}

// BaseElement * BaseMesh::createCellFromNodeIdxVector( const vector < long > & idx, double attribute ){
//   vector < Node * > pNodeVector;

//   switch( idx.size() ){
//   case 3: //** 3-node triangle
//     return dynamic_cast< Mesh2D & >( *this ).createTriangle( node( idx[ 0 ] ), node( idx[ 1 ] ),
// 					      node( idx[ 2 ] ), cellCount(), attribute );
//     break;
//   case 4: //** 4-node tetrahedron
//     pNodeVector.clear();
//     for ( int i = 0; i < 4; i ++ ) pNodeVector.push_back( &node( idx[ i ] ) );
//     return dynamic_cast< Mesh3D & >( *this ).createTetrahedron( pNodeVector, cellCount(), attribute);
//     break;
//   case 6: //** 6-node triangle
//     pNodeVector.clear();
//     for ( int i = 0; i < 6; i ++ ) pNodeVector.push_back( &node( idx[ i ] ) );
//     return dynamic_cast< Mesh2D & >( *this ).createTriangle6( pNodeVector, cellCount(), attribute);
//     break;
//   case 7: //** 3 node triangle old style with neighbourgs
//     return dynamic_cast< Mesh2D & >( *this ).createTriangle( node( idx[ 1 ] ), node( idx[ 2 ] ),
// 							     node( idx[ 3 ] ), cellCount(), attribute );
//     break;
//   case 10: //** 10 node tetrahedron
//     pNodeVector.clear();
//     for ( int i = 0; i < 10; i ++ ) pNodeVector.push_back( &node( idx[ i ] ) );
//     return dynamic_cast< Mesh3D & >( *this ).createTetrahedron( pNodeVector, cellCount(), attribute );
//     break;
//   default:
//     cerr << WHERE_AM_I << " cell type must be defined " << idx.size() << endl; exit( 1 );
//   }
//   return NULL;
// }

// BaseElement * BaseMesh::createBoundaryFromNodeIdxVector( const vector < long > & idx, int marker ){
//   vector < Node * > pNodeVector;
//   switch( idx.size() ){
//   case 2:
//     dynamic_cast< Mesh2D & >( *this ).createEdge( node( idx[ 0 ] ), node( idx[ 1 ] ), boundaryCount(), marker );
//     break;
//   case 3: // 3 node triangle
//     switch ( dim() ){
//     case 2:
//       dynamic_cast< Mesh2D & > (*this).createEdge3( node( idx[ 0 ] ), node( idx[ 1 ] ),
// 						    node( idx[ 2 ] ), boundaryCount(), marker );
//       break;
//     case 3:
//       for ( int i = 0; i < 3; i ++ ) pNodeVector.push_back( &node( idx[ i ] ) );
//       dynamic_cast< Mesh3D & > (*this).createTriangleFace( pNodeVector, boundaryCount(), marker );
//       break;
//     }
//     break;
//   case 6: // 6 node triangle
//     for ( int i = 0; i < 6; i ++ ) pNodeVector.push_back( &node( idx[ i ] ) );
//     dynamic_cast< Mesh3D & > (*this).createTriangleFace( pNodeVector, boundaryCount(), marker );
//     break;
//   default:
//     cerr << WHERE_AM_I << " cell type must be defined " << idx.size() << endl; exit( 1 );
//   }
//   return NULL;
// }

int BaseMesh::save( const string & fbody, const string & style, IOFormat format ){
  if ( format == Binary || fbody.find( MESHBINSUFFIX ) != std::string::npos ) return saveBinary( fbody );
  if ( style != "ndef" ){
    if ( style == "DCFEMLib" || style == "d" || style == "D" ) {
      return saveDCFEMLibMesh( fbody );
    } else {
      cerr << "Outputformat: " << style << " unknown" << endl; return 0;
    }
  } else {
    cerr << "Outputformat not defined" << endl; return 0;
  }
}

int BaseMesh::load( const string & fbody, const string & style, IOFormat format ){
  //  cout << fbody << " " << style << " " << format << endl;
  if ( format == Binary || fbody.find( MESHBINSUFFIX ) != (size_t)-1 ) return loadBinary( fbody );

  if ( style != "ndef" ){
    if ( style == "DCFEMLib" || style == "d" || style == "D" || style == "m" || style == "M" ){
      return loadDCFEMLibMesh( fbody );
    } else if ( style == "Grummp" || style == "g" || style == "G" ){
      return loadGrummpMesh( fbody );
    } else {
      cerr << "Intputformat: " << style << " unknown" << endl; return 0;
    }
  } else {
    cerr << "Inputformat not defined, Autodetection not yet implemented" << endl;
    return 0;
  }
  return 0;
}


int BaseMesh::saveDCFEMLibMesh( const string & fbody ){
  int count = 0;
  recount();

  if ( saveNodes( fbody + ".n" ) ) count++;
  if ( saveCells( fbody + ".e" ) ) count++;

  switch( dim() ){
  case 2:
    if ( saveBoundaries( fbody + ".s" ) ) count++;
    break;
  case 3:
    if ( saveBoundaries( fbody + ".f" ) ) count++;
    break;
  }

  if ( count != 3 ){
    cerr << WHERE_AM_I << " something goes wrong." << endl;  return 0;
  }
  return 1;
}

int BaseMesh::loadDCFEMLibMesh(const string & fbody){
  clear();

  int count = 0;
  if ( loadNodes(fbody + ".n") ) count++;
  if ( loadCells(fbody + ".e") ) count++;

  switch( dim() ){
  case 2:
    if ( loadBoundaries( fbody + ".s" ) ) count++;
    break;
  case 3:
    if ( loadBoundaries( fbody + ".f" ) ) count++;
    break;
  }

//   if (count < 2 ){
//     cerr << WHERE_AM_I << " Unable to load Mesh.\n" << endl;
//     return 0;
//   }

  return 1;
}

int BaseMesh::saveBinary( const string & fname ){

  string fileName( fname.substr( 0, fname.rfind( MESHBINSUFFIX ) ) + MESHBINSUFFIX );

  FILE *file;
  file = fopen( fileName.c_str(), "w+b" );

  int dimension = dim(); fwrite( &dimension, sizeof( int ), 1, file );

  //** write vertex dummy-infos;
  int dummy[ 127 ]; memset( dummy, 0, 127 * sizeof(int) ); fwrite( dummy, sizeof(int), 127, file );

  int nVerts = nodeCount(); fwrite( &nVerts, sizeof(int), 1, file );

  double *koord = new double[ dimension * nVerts ];
  for ( int i = 0; i < nVerts; i ++ ){
    for ( int j = 0; j < dimension; j ++ ){
      koord[ i * dimension + j ] = node( i ).pos()[ j ];
    }
  }
  fwrite( koord, sizeof(double), dimension * nVerts, file );
  delete [] koord;

  int * marker = new int[ nVerts ];
  for ( int i = 0; i < nVerts; i ++ ) marker[ i ] = node( i ).marker();
  fwrite( marker, sizeof( int ), nVerts, file );
  delete [] marker;

  //** write cell dummy-infos
  fwrite( dummy, sizeof( int ), 127, file );

  int nCells = cellCount(); fwrite( &nCells, sizeof( int ), 1, file );

  int * cellVerts = new int[ nCells ];
  int nCellIdx = 0;

  for ( int i = 0; i < nCells; i ++ ){
    cellVerts[ i ] = cell( i ).nodeCount();
    nCellIdx += cellVerts[ i ];
  }
  fwrite( cellVerts, sizeof(int), nCells, file );

  int * idx = new int[ nCellIdx ];

  int count = 0;
  for ( int i = 0; i < nCells; i ++ ){
    for ( int j = 0; j < cellVerts[ i ]; j ++ ){
      idx[ count ] = cell( i ).node( j ).id();
      count ++;
    }
  }
  fwrite( idx, sizeof(int), nCellIdx, file );
  delete [] idx;
  delete [] cellVerts;

  double * attribute = new double[ nCells ];
  for ( int i = 0; i < nCells; i ++ ) attribute[ i ] = cell( i ).attribute();
  fwrite( attribute, sizeof( double ), nCells, file );
  delete [] attribute;

  //** write boundary dummy-infos
  fwrite( dummy, sizeof(int), 127, file );

  int nBounds = boundaryCount(); fwrite( &nBounds, sizeof(int), 1, file );

  int * boundVerts = new int[ nBounds ];
  int nBoundIdx = 0;
  for ( int i = 0; i < nBounds; i ++ ){
    boundVerts[ i ] = boundary( i ).nodeCount();
    nBoundIdx += boundVerts[ i ];
  }
  fwrite( boundVerts, sizeof(int), nBounds, file );

  idx = new int[ nBoundIdx ];
  count = 0;
  for ( int i = 0; i < nBounds; i ++ ){
    for ( int j = 0; j < boundVerts[ i ]; j ++ ){
      idx[ count ] = boundary( i ).node( j ).id();
      count ++;
    }
  }
  fwrite( idx, sizeof(int), nBoundIdx, file );
  delete [] idx;
  delete [] boundVerts;

  marker = new int[ nBounds ];
  for ( int i = 0; i < nBounds; i ++ ) marker[ i ] = boundary( i ).marker();
  fwrite( marker, sizeof( int ), nBounds, file );
  delete [] marker;

  idx = new int[ nBounds ];
  for ( int i = 0; i < nBounds; i ++ ){
    if ( boundary( i ).leftCell() != NULL ){
      idx[ i ] = boundary( i ).leftCell()->id();
    } else {
      idx[ i ] = -1;
    }
  }
  fwrite( idx, sizeof( int ), nBounds, file );

  for ( int i = 0; i < nBounds; i ++ ){
    if ( boundary( i ).rightCell() != NULL ){
      idx[ i ] = boundary( i ).rightCell()->id();
    } else {
      idx[ i ] = -1;
    }
  }
  fwrite( idx, sizeof( int ), nBounds, file );
  delete [] idx;

  fclose( file );
  return 1;
}

int BaseMesh::loadBinary( const string & fname ){
//   sizeof( int ) = 4 byte
//   int[ 1 ] dimension
//   int[ 127 ] dummy vertices information
//   int[ 1 ] nVerts, number of vertices
//   double[ dimension * nVerts ]; koordinates,  dimension == 2 ( x, y ), dimension == 3 (x, y, z )
//   int[ nVerts ] vertex markers
//   int[ 127 ] dummy cell information
//   int[ 1 ] nCells, number of cell
//   int[ nCells ] cellVerts; number of nodes for each cell
//   int[ sum( cellVerts ) ] cellsidx
//   double[ nCells ] attribute, cell attributes
//   int[ 127 ] dummy boundary information
//   int[ 1 ] nBounds, number of boundarys
//   int[ nBounds ] boundVerts; numbers of nodes for each boundary
//   int[ sum( boundVerts ) ] boundIdx
//   int[ nBounds ] boundary markers
//   int[ nBounds ] leftNeighbour idx (-1) if no neighbour present or info unavailable
//   int[ nBounds ] rightNeighbour idx (-1) if no neighbour present or info unavailable

  bool debug = false;
  clear();
  string fileName( fname.substr( 0, fname.rfind( MESHBINSUFFIX ) ) + MESHBINSUFFIX );

  FILE *file;
  file = fopen( fileName.c_str(), "r+b" );
  if ( file == NULL ) { cerr << fileName << ": " << strerror( errno ) << endl;  return 0; }

  uint32 dimension; fread( &dimension, sizeof( uint32 ), 1, file );
  if ( (size_t)dim() != dimension ) cerr << WHERE_AM_I << " warning "<< dim()
				 << "d mesh initialized but " << dimension << "d mesh loading" << endl;

  if ( debug ) std::cout  << "Dim: " << dimension << std::endl;
  //** read vertex dummy-infos;
  uint32 dummy[ 127 ]; fread( dummy, sizeof( uint32 ), 127, file );
  uint32 nVerts; fread( &nVerts, sizeof( uint32 ), 1, file );
  if ( debug ) std::cout << "nVerts: " << nVerts << std::endl;

  double * koords = new double[ dimension * nVerts ]; fread( koords, sizeof( double ), dimension * nVerts, file );
  int32 * nodeMarker = new int32[ nVerts ]; fread( nodeMarker, sizeof( int32 ), nVerts, file );

  //** read cell dummy-infos
  fread( dummy, sizeof( uint32 ), 127, file );

  uint32 nCells; fread( &nCells, sizeof( uint32 ), 1, file );
  if ( debug ) std::cout  << "nCells: "  << nCells << std::endl;

  uint32 *   cellVerts = new uint32[ nCells ]; fread( cellVerts, sizeof( uint32 ), nCells, file );
  uint32     nCellIdx = 0; for ( uint i = 0; i < nCells; i ++ ) nCellIdx += cellVerts[ i ];
  uint32 *      cellIdx = new uint32[ nCellIdx ]; fread( cellIdx, sizeof( uint32 ), nCellIdx, file );
  double * attribute = new double[ nCells ]; fread( attribute, sizeof( double ), nCells, file );

  //** read boundary dummy-infos
  fread( dummy, sizeof( uint32 ), 127, file );

  uint32 nBounds; fread( &nBounds, sizeof( uint32 ), 1, file );
  if ( debug ) std::cout <<  "nBounds: " <<nBounds << std::endl;
  uint32 * boundVerts = new uint32[ nBounds ]; fread( boundVerts, sizeof( uint32 ), nBounds, file );
  uint32   nBoundIdx = 0; for ( uint i = 0; i < nBounds; i ++ ) nBoundIdx += boundVerts[ i ];
  uint32 * boundIdx    = new uint32[ nBoundIdx ]; fread( boundIdx,    sizeof( int32 ), nBoundIdx, file );
  int32  * boundMarker = new int32[ nBounds ];    fread( boundMarker, sizeof( int32 ), nBounds, file );
  int32  * left  = new int32[ nBounds ]; fread( left,  sizeof( int32 ), nBounds, file );
  int32  * right = new int32[ nBounds ]; fread( right, sizeof( int32 ), nBounds, file );

  //** create Nodes
  for ( uint i = 0; i < nVerts; i ++ ){
    createNode( RealPos( 0.0, 0.0, 0.0 ), nodeCount() );
    for ( uint j = 0; j < dimension; j ++ ){
      node( i ).pos()[ j ] = koords[ i * dimension + j ];
    }
    node( i ).setMarker( nodeMarker[ i ] );
  }
  //** create Cells;
  int count = 0;
  vector < Node * > pNodeVector;

  for ( uint i = 0; i < nCells; i ++ ){
    //    if ( debug ) std::cout << "Cell: " << i << " " << cellVerts[ i ] << std::endl;

    switch( cellVerts[ i ] ){
    case 3: //** 3 node triangle
      dynamic_cast< Mesh2D & >( *this ).createTriangle( node( cellIdx[ count ] ),
						      node( cellIdx[ count + 1 ] ),
						      node( cellIdx[ count + 2 ] ),
						      cellCount(), 0.0 );
      count += 3;
      break;
    case 4: //** 4 node tetrahedron
      pNodeVector.clear();
      if ( dim() == 3 ){
	for ( int j = 0; j < 4; j ++ ) pNodeVector.push_back( &node( cellIdx[ count + j ] ) );

	dynamic_cast< Mesh3D & >( *this ).createTetrahedron( pNodeVector, cellCount(), 0.0 );
	count += 4;
      } else{
	TO_IMPL
      }
      break;
    case 6: //** 6 node triangle
      pNodeVector.clear();
      for ( int j = 0; j < 6; j ++ ) pNodeVector.push_back( &node( cellIdx[ count + j ] ) );
      dynamic_cast< Mesh2D & >( *this ).createTriangle6( pNodeVector, cellCount(), 0.0 );
      count += 6;
      break;
    case 10: //** 10 node tetrahedron
      pNodeVector.clear();
      for ( int j = 0; j < 10; j ++ ) pNodeVector.push_back( &node( cellIdx[ count + j ] ) );

      dynamic_cast< Mesh3D & >( *this ).createTetrahedron( pNodeVector, cellCount(), 0.0 );
      count += 10;
      break;
    default:
      cerr << WHERE_AM_I << " cell type must be defined " << cellVerts[ i ] << endl; exit( 1 );
    }
  }
  for ( uint i = 0; i < nCells; i ++ ) cell( i ).setAttribute( attribute[ i ] );

  //** create Boundaries;
  count = 0;
  for ( uint i = 0; i < nBounds; i ++ ){
    switch( boundVerts[ i ] ){
    case 2:
      dynamic_cast<Mesh2D &>( *this ).createEdge( node( boundIdx[ count ] ),
						  node( boundIdx[ count + 1 ] ),
						  boundaryCount(), 0 );
      count += 2;
      break;
    case 3: // 3 node triangle-face
      switch ( dimension )  {
      case 2:
	dynamic_cast< Mesh2D & >( *this ).createEdge3( node( boundIdx[ count ] ),
						       node( boundIdx[ count + 1 ] ),
						       node( boundIdx[ count + 2 ] ),
						       boundaryCount(), 0 );
	break;
      case 3:
	pNodeVector.clear();
	dynamic_cast< Mesh3D & >( *this ).createTriangleFace( node( boundIdx[ count ] ),
							      node( boundIdx[ count + 1 ] ),
							      node( boundIdx[ count + 2 ] ),
							      boundaryCount(), 0 );
	break;
      }
      count += 3;
      break;
    case 6: // 6 node triangle-face
      pNodeVector.clear();
      for ( int j = 0; j < 6; j ++ ) pNodeVector.push_back( &node( boundIdx[ count + j ] ) );
      dynamic_cast< Mesh3D & > (*this).createTriangleFace( pNodeVector, boundaryCount(), 0 );
      count += 6;
      break;

    default:
      cerr << WHERE_AM_I << " boundary type must be defined " << boundVerts[ i ] << endl; exit( 1 );
    }
  }
  for ( uint i = 0; i < nBounds; i ++ ){
    boundary( i ).setMarker( boundMarker[ i ] );
    if ( left[ i ] != -1 )  boundary( i ).setLeftCell( cell( left[ i ] ) );
    if ( right[ i ] != -1 ) boundary( i ).setRightCell( cell( right[ i ] ) );
  }


  delete [] koords;
  delete [] nodeMarker;

  delete [] cellIdx;
  delete [] cellVerts;
  delete [] attribute;

  delete [] boundIdx;
  delete [] boundVerts;
  delete [] boundMarker;
  delete [] left;
  delete [] right;

  fclose( file );
  return 1;
}

int BaseMesh::saveNodes(const string & fname){
  fstream file; if ( !openOutFile( fname, & file ) ) {  return 0; }

  file.precision( 14 );
  for (VectorpNodes::iterator it = pNodeVector_.begin(); it != pNodeVector_.end(); it++){
    for ( int i = 0; i < dim(); i ++ ) file << (*it)->pos()[ i ] << "\t";
    file << (*it)->marker() << endl;
  }

  file.close();
  return 1;
}

int BaseMesh::loadNodes( const string & fname ){
  fstream file; if ( ! openInFile( fname, & file ) ) return 0;

  vector < string > row;
  while ( (row = getNonEmptyRow( file ) ).size() > 0 ){
    switch ( row.size() ){
    case 3:
      createNode( toDouble( row[ 0 ] ), toDouble( row[ 1 ] ), 0.0, -1, toInt( row[ 2 ] ) );
      break;
    case 4:
      createNode( toDouble( row[ 0 ] ), toDouble( row[ 1 ] ), toDouble( row[ 2 ] ), -1, toInt( row[ 3 ] ) );
      break;
    default:
      cerr << WHERE_AM_I << " cannot determine dataformat: " << row.size() << endl;
      return 0;
    }
  }
  file.close();
  return 1;
}

int BaseMesh::saveCells( const string & fname ){
  fstream file; if ( !openOutFile( fname, & file ) ) { return 0; }

  for ( int i = 0, imax = cellCount(); i < imax; i++){
    for ( int j = 0, jmax = cell( i ).nodeCount(); j < jmax; j++){
      file << cell( i ).node( j ).id() << "\t" ;
    }
    file << cell( i ).attribute() << endl;
  }
  file.close();
  return 1;
}

int BaseMesh::loadCells( const string & fname ){
  fstream file; if ( !openInFile( fname, & file ) ) return 0;

  vector < string > row; row = getRowSubstrings( file );
  int columnCount = row.size();
  file.seekg( 0 );

  vector < long > idx( columnCount - 1 );
  double attribute = 0.0;
  while ( file >> idx[ 0 ] ){
    for ( int i = 1; i < columnCount - 1; i ++ ){
      file >> idx[ i ];
    }
    file >> attribute;
    createCellFromNodeIdxVector( idx, attribute );
  }

  file.close();
  return 1;
}

int BaseMesh::saveBoundaries( const string & fname ){
  fstream file; if ( !openOutFile( fname, & file ) ) { return 0; }

  for ( int i = 0, imax = boundaryCount(); i < imax; i++){
    for ( int j = 0, jmax = boundary( i ).nodeCount(); j < jmax; j++){
      file << boundary( i ).node( j ).id() << "\t" ;
    }
    file << "-33\t-33\t" << boundary( i ).marker() << endl;
  }
  file.close();
  return 1;
}

int BaseMesh::loadBoundaries( const string & fname ){
  fstream file; if ( !openInFile( fname, & file ) ) return 0;

  vector < string > row; row = getRowSubstrings( file );
  int columnCount = row.size();
  file.seekg( 0 );
  int marker = 0, dummy = 0;

    if ( columnCount == 6 && dim() == 2){ // old myfemlibformat nearly obsolete
    vector < long > idx( columnCount - 4 );
    while ( file >> dummy ){
      for ( int i = 0; i < columnCount - 4; i ++ ){
	file >> idx[ i ];
      }
      file >> dummy >> dummy >> marker;
      createBoundaryFromNodeIdxVector( idx, marker );
    }
  } else if ( columnCount > 2 ){
    vector < long > idx( columnCount - 3 );

    while ( file >> idx[ 0 ] ){
      for ( int i = 1; i < columnCount - 3; i ++ ){
	file >> idx[ i ];
      }
      file >> dummy >> dummy >> marker;
      createBoundaryFromNodeIdxVector( idx, marker );
    }
  } else {
    cerr << WHERE_AM_I << " boundary file corrupt or empty, ignoring. " << columnCount << endl;
  }
  showInfos();

  file.close();
  return 1;
}

// int BaseMesh::saveEdges(const string & fname){
//   fstream file;  if ( !openOutFile( fname, & file ) ) {  return 0; }

//   /* Nr.      Node(A)       Node(B)      ElementLeft      ElementRight     Marker*/
//   for (VectorpEdges::iterator it = pBoundaryVector_.begin(); it != pBoundaryVector_.end(); it++){
//     switch ( (*it)->rtti() ){
//     case MYMESH_EDGE3_RTTI:
//       //file << (*(*it)) << endl; break;
//       file << (*it)->id()  << "\t"
// 	   << (*it)->node( 0 ).id() << "\t"
// 	   << (*it)->node( 1 ).id() << "\t"
// 	   << (*it)->node( 2 ).id() << "\t";
//       break;
//     case MYMESH_EDGE_RTTI:
//       //file << (*(*it)) << endl; break;
//       file << (*it)->id()  << "\t"
// 	   << (*it)->node( 0 ).id() << "\t"
// 	   << (*it)->node( 1 ).id() << "\t";
//       break;
//     default: cerr << WHERE_AM_I << " format not know for " << (*it)->rtti() << endl;
//     }
//     if ( (*it)->leftCell() != NULL) file << (*it)->leftCell()->id() << "\t";
//     else file << "-1\t";
//     if ( (*it)->rightCell() != NULL) file << (*it)->rightCell()->id() << "\t";
//     else file << "-1\t";
//     file << (*it)->marker()<< endl;
//   }
//   file.close();
//   return 1;
// }


// int BaseMesh::loadEdges(const string & fname){;
//   int columncount = countColumns( fname );

//   fstream file; if ( !openInFile( fname, & file ) ) {  return 0; }

//   int id = -1, a = -1, b = -1, c = -1, el = -1, er = -1, marker = -1;
//   Edge * edge;
//   switch ( columncount ){
//   case 1:
//     // do nothing
//     break;
//   case 6: // found edge file
//     while(file >> id >> a >> b >> el >> er >> marker){
//       edge = createEdge( node(a), node(b), id, marker);
//     }
//     break;
//   case 7: // found edge3 file
//     while(file >> id >> a >> b >> c >> el >> er >> marker){
//       edge = createEdge3( node(a), node(b), node(c), id, marker);
//     }
//     break;
//   default:
//     break;
//     //    cerr << WHERE_AM_I << " format not known. " << endl;
//   }
//   file.close();
//   return 1;
// }

void BaseMesh::recount(){;
  int i = 0;
  VectorpNodes tmp(pNodeVector_);
  pNodeVector_.clear();

  for ( int j = 0, jmax = tmp.size(); j < jmax; j ++) {
    if ( tmp[ j ] != NULL ) {
      pNodeVector_.push_back( tmp[ j ] );
      pNodeVector_[ i ]->setId( i );
      i ++;
    }
  }
  i = 0;
  for (VectorpElements::iterator it = pBoundaryVector_.begin(); it != pBoundaryVector_.end(); it++, i++){
    (*it)->setId(i);
  }
  i = 0;
  for (VectorpElements::iterator it = pCellVector_.begin(); it != pCellVector_.end(); it++, i++){
    (*it)->setId(i);
  }
}

void BaseMesh::setRefineSelectList(const VectorpElements & refine){
  refineList.clear();
  refineList = refine;
}

void BaseMesh::refine(const VectorpElements & toRefine){
  if (toRefine.size() > 0) setRefineSelectList(toRefine);
  refine();
}

void BaseMesh::clearSubInfos(){
  _edgeSubNode.clear();
  _elementRefineStatus.clear();
}

void BaseMesh::findWorldKoordinates(){

  if ( nodeCount() > 0 ){
    xmin_ = 9e99; ymin_ = xmin_; zmin_ = xmin_;
    xmax_ = -xmin_; ymax_ = xmax_; zmax_ = xmax_;
//     xmin_ = node( 0 ).x(); ymin_ = node( 0 ).y(); zmin_ = node( 0 ).z();
//     xmax_ = xmin_; ymax_ = ymin_; zmax_ = zmin_;

    double tmp;
    for (int i = 1; i < nodeCount(); i ++){
      tmp = node(i).x();
      if (tmp < xmin_) xmin_ = tmp;
      if (tmp > xmax_) xmax_ = tmp;

      tmp = node(i).y();
      if (tmp < ymin_) ymin_ = tmp;
      if (tmp > ymax_) ymax_ = tmp;

      tmp = node(i).z();
      if (tmp < zmin_) zmin_ = tmp;
      if (tmp > zmax_) zmax_ = tmp;
    }
    //    if (xmin_ == xmax_){ xmin_ = 0.0; xmax_ = 1.0; }
    //    if (ymin_ == ymax_){ ymin_ = 0.0; ymax_ = 1.0; }
    //    if (zmin_ == zmax_){ zmin_ = 0.0; zmax_ = 1.0; }
  }
  // else {
//     xmin_ = 0.0; xmax_ = 1.0;
//     ymin_ = 0.0; ymax_ = 1.0;
//     zmin_ = 0.0; zmax_ = 1.0;
//   }
 _isworldmatrixknown = 1;
}

double BaseMesh::worldKoordinate( int pos ) {
  // Q & D sehr Haesslich
  if (_isworldmatrixknown != 1){
    //   xmin_ = xmax_ = ymin_ = ymax_ = zmin_ = zmax_ = 0.0;
    findWorldKoordinates();
  }

  switch ( pos ){
  case 1: return xmin_;
  case 2: return xmax_;
  case 3: return ymin_;
  case 4: return ymax_;
  case 5: return zmin_;
  case 6: return zmax_;
  }
  return -1.1111111; // :/
}

vector< int > BaseMesh::getAllMarkedNodes( int marker ) const {
  vector < int > nodes;
  for ( int i = 0, imax = nodeCount(); i < imax ;i ++ ){
    if ( node( i ).marker() == marker ) nodes.push_back( i );
  }
  return nodes;
}

vector < int > BaseMesh::cellIndex() const {
  vector < int > index;
  for ( size_t i = 0, imax = cellCount(); i < imax; i ++ ){
    index.push_back( (size_t)cell( i ).attribute() );
  }
  return index;
}

void BaseMesh::mapCellAttributes( const map < float, float > & aMap ){
  map< float, float >::const_iterator itm;
  vector< BaseElement * > emptyList;

  if ( aMap.size() != 0 ){
    for ( unsigned int i = 0, imax = cellCount(); i < imax; i++ ){
      itm = aMap.find( cell( i ).attribute() );
      if ( itm != aMap.end() ){
	cell( i ).setAttribute( (*itm).second );
	if ( cell( i ).attribute() == 0.0 ) emptyList.push_back( &cell( i ) );
      }
    }
    fillEmptyCells( emptyList );
  }
}

void BaseMesh::mapCellAttributes( const vector< int > & cellMapIndex, const RVector & attributeMap, double defaultVal ){
  //** cellMapIndex[ cell ] == 1 -> backgrund cell
  //** cellMapIndex[ cell ] > 1 -> model cell
  //** cellMapIndex[ cell ] == 2 -> attributeMap[ 0 ] cell
  vector< BaseElement * > emptyList;

  for ( size_t i = 0, imax = cellCount(); i < imax; i ++ ){
    if ( cellMapIndex[ i ] == 1 ){
      if ( defaultVal == 0.0 ) emptyList.push_back( &cell( i ) );
      cell( i ).setAttribute( defaultVal );
    } else {
      cell( i ).setAttribute( attributeMap[ cellMapIndex[ i ] - 2 ] );
    }
  }
  fillEmptyCells( emptyList );
}

vector< SetpCells > neighbourList_;

void BaseMesh::fillEmptyCells( vector< BaseElement * > & emptyList ){
  if ( emptyList.size() > 0 ){
    //    cout << "Fill " << emptyList.size() << " empty cells." << endl;
    vector< BaseElement *> nextVector;
    SetpCells neighbours;
    vector< BaseElement * > toFill;

    int count = 0;
    double val = 0.0;
    if ( neighbourList_.size() != (size_t)cellCount() ) neighbourList_.resize( cellCount() );

    for ( size_t i = 0; i < emptyList.size(); i ++){
      if ( neighbourList_[ emptyList[ i ]->id() ].size() == 0 ){
	neighbourList_[ emptyList[ i ]->id() ] = findNeighbours( *emptyList[ i ] );
      }
      neighbours = neighbourList_[ emptyList[ i ]->id() ];
      count = 0;
//       val = 0.0;
      for ( SetpCells::iterator itc = neighbours.begin(); itc != neighbours.end(); itc ++ ){
	if ( (*itc)->attribute() != 0.0 ){
	  toFill.push_back( emptyList[ i ] );
	  // 	  val += (*itc)->attribute();
 	  count ++;
	}
      }
      if ( count == 0 ) nextVector.push_back( emptyList[ i ] );
    }
    for ( size_t i = 0; i < toFill.size(); i ++){
      count = 0;
      val = 0.0;
      neighbours = neighbourList_[ toFill[ i ]->id() ];
      for ( SetpCells::iterator itc = neighbours.begin(); itc != neighbours.end(); itc ++ ){
	if ( (*itc)->attribute() != 0.0 ){
	  val += (*itc)->attribute();
	  count ++;
	}
      }
      toFill[ i ]->setAttribute( val / (double)count );
    }
//     char strdummy[128];
//     sprintf(strdummy, "tmp%d",emptyList.size());
//     save(strdummy);

    fillEmptyCells( nextVector );
  }
  //  neighbourList_.clear();
}

SetpCells BaseMesh::findNeighbours( BaseElement & cell ) const {
  SetpCells commonCellsF1;
  SetpCells commonCellsF2;
  SetpCells commonCellsF3;
  SetpCells commonCellsF4;
  SetpCells commonCells;

  switch( cell.rtti() ){
  case MYMESH_TETRAHEDRON_RTTI:
    commonCellsF1 = cell.node( 0 ).cellSet() & cell.node( 1 ).cellSet() & cell.node( 2 ).cellSet();
    commonCellsF2 = cell.node( 0 ).cellSet() & cell.node( 1 ).cellSet() & cell.node( 3 ).cellSet();
    commonCellsF3 = cell.node( 1 ).cellSet() & cell.node( 2 ).cellSet() & cell.node( 3 ).cellSet();
    commonCellsF4 = cell.node( 2 ).cellSet() & cell.node( 3 ).cellSet() & cell.node( 0 ).cellSet();
    commonCells = commonCellsF1 + commonCellsF2 + commonCellsF3 + commonCellsF4;
    commonCells.erase( &cell );
    break;
  case MYMESH_TETRAHEDRON10_RTTI:
    commonCellsF1 = cell.node( 0 ).cellSet() & cell.node( 1 ).cellSet() & cell.node( 2 ).cellSet();
    commonCellsF2 = cell.node( 0 ).cellSet() & cell.node( 1 ).cellSet() & cell.node( 3 ).cellSet();
    commonCellsF3 = cell.node( 1 ).cellSet() & cell.node( 2 ).cellSet() & cell.node( 3 ).cellSet();
    commonCellsF4 = cell.node( 2 ).cellSet() & cell.node( 3 ).cellSet() & cell.node( 0 ).cellSet();
    commonCells = commonCellsF1 + commonCellsF2 + commonCellsF3 + commonCellsF4;
    commonCells.erase( &cell );
    break;
  case MYMESH_TRIANGLE_RTTI:
    commonCellsF1 = cell.node( 0 ).cellSet() & cell.node( 1 ).cellSet();
    commonCellsF2 = cell.node( 1 ).cellSet() & cell.node( 2 ).cellSet();
    commonCellsF3 = cell.node( 2 ).cellSet() & cell.node( 0 ).cellSet();
    commonCells = commonCellsF1 + commonCellsF2 + commonCellsF3;
    commonCells.erase( &cell );
    break;
  case MYMESH_TRIANGLE6_RTTI:
    commonCellsF1 = cell.node( 0 ).cellSet() & cell.node( 1 ).cellSet();
    commonCellsF2 = cell.node( 1 ).cellSet() & cell.node( 2 ).cellSet();
    commonCellsF3 = cell.node( 2 ).cellSet() & cell.node( 0 ).cellSet();
    commonCells = commonCellsF1 + commonCellsF2 + commonCellsF3;
    commonCells.erase( &cell );
    break;
  default:
    TO_IMPL
      cerr << cell.rtti() << endl;
      exit(0);
  }

//   show( commonCellsF1 ) ;
//   show( commonCellsF2 ) ;
//   show( commonCellsF3 ) ;
//   show( commonCells) ;
  return commonCells;
}

void BaseMesh::mapBoundaryConditions( const map < float, float > & bMap ){;
  map< float, float >::const_iterator itm;
  if ( bMap.size() != 0 ){
    for ( unsigned int i = 0, imax = boundaryCount(); i < imax; i++ ){
      itm = bMap.find( boundary( i ).marker() );
      if ( itm != bMap.end() ){
	boundary( i ).setMarker( (int)(*itm).second );
      }
    }
  }
}

BaseElement & BaseMesh::findNearestCell( const RealPos & pos ) const {
  map< double, BaseElement * > sortMap;
  for ( int i = 0; i < cellCount(); i ++ ){
    sortMap[ pos.distance( cell(i).middlePos() ) ] = &cell(i);
  }

  return *sortMap.begin()->second;
}

void BaseMesh::findSubMeshByIdx( const BaseMesh & mesh, vector < int > & idxList ){

  this->clear();
  map < int, int > nodeMap;
  int count = 0;

  for ( size_t i = 0; i < idxList.size(); i ++ ){
    for ( int j = 0; j < mesh.cell( idxList[ i ] ).nodeCount(); j ++ ){
      if ( nodeMap.count( mesh.cell( idxList[ i ] ).node( j ).id() ) == 0 ){

	nodeMap[ mesh.cell( idxList[ i ] ).node( j ).id() ] = count;
	this->createNode( mesh.cell( idxList[ i ] ).node( j ).pos(), count, mesh.cell( idxList[ i ] ).node( j ).marker() );
	count ++;
      }
    }
  }

  int edgeMarker = 0;
  Node *n1, *n2, *n3, *n4;

  for ( size_t i = 0; i < idxList.size(); i ++ ){

    switch( mesh.dim() ){
    case 2:
      n1 = &this->node( nodeMap[ mesh.cell( idxList[ i ] ).node( 0 ).id() ] );
      n2 = &this->node( nodeMap[ mesh.cell( idxList[ i ] ).node( 1 ).id() ] );
      n3 = &this->node( nodeMap[ mesh.cell( idxList[ i ] ).node( 2 ).id() ] );

      dynamic_cast < Mesh2D *>(this)->createTriangle( *n1, *n2, *n3, i, mesh.cell( idxList[ i ] ).attribute() );

//edgeMarker = mesh.findEdge( *n0, *n1 );
//n0.findEdge( n1 );
      if ( dynamic_cast < Mesh2D *>(this)->findEdge( *n1, *n2 ) == NULL ) {
          BaseElement * e = dynamic_cast < Mesh2D *>(this)->createEdge( *n1, *n2 );
          Edge * e1 = mesh.cell( idxList[ i ] ).node( 0 ).findEdge( mesh.cell( idxList[ i ] ).node( 1 ) );
            if ( e && e1 ) e->setMarker( e1->marker() );
        }
      if ( dynamic_cast < Mesh2D *>(this)->findEdge( *n2, *n3 ) == NULL ) {
            BaseElement * e = dynamic_cast < Mesh2D *>(this)->createEdge( *n2, *n3 );
          Edge * e1 = mesh.cell( idxList[ i ] ).node( 1 ).findEdge( mesh.cell( idxList[ i ] ).node( 2 ) );
            if ( e && e1 ) e->setMarker( e1->marker() );
    }
      if ( dynamic_cast < Mesh2D *>(this)->findEdge( *n3, *n1 ) == NULL ) {
          BaseElement * e = dynamic_cast < Mesh2D *>(this)->createEdge( *n3, *n1 );
          Edge * e1 = mesh.cell( idxList[ i ] ).node( 2 ).findEdge( mesh.cell( idxList[ i ] ).node( 0 ) );
            if ( e && e1 ) e->setMarker( e1->marker() );
    }

      break;
    case 3:
      n1 = &this->node( nodeMap[ mesh.cell( idxList[ i ] ).node( 0 ).id() ] );
      n2 = &this->node( nodeMap[ mesh.cell( idxList[ i ] ).node( 1 ).id() ] );
      n3 = &this->node( nodeMap[ mesh.cell( idxList[ i ] ).node( 2 ).id() ] );
      n4 = &this->node( nodeMap[ mesh.cell( idxList[ i ] ).node( 3 ).id() ] );

      dynamic_cast < Mesh3D *>(this)->createTetrahedron( *n1, *n2, *n3, *n4, i, mesh.cell( idxList[ i ] ).attribute() );

      //      createTriangleFace_( Node & a, Node & b, Node & c, int id, int marker );

      break;
    }
  }
}

void BaseMesh::findSubMeshByAttribute( const BaseMesh & mesh, double from, double to ){
  vector < int > cellIdx;
  if ( to == -1 ) to = from;

  for ( int i = 0; i < mesh.cellCount(); i ++ ){
    if ( mesh.cell( i ).attribute() >= from && mesh.cell( i ).attribute() <= to ){
      cellIdx.push_back( i );
    }
  }
  findSubMeshByIdx( mesh, cellIdx);


//   this->clear();
//   if ( to == -1 ) to = from;
//   map < int, int > nodeMap;
//   int count = 0;

//   for ( int i = 0; i < mesh.cellCount(); i ++ ){
//     if ( mesh.cell( i ).attribute() >= from && mesh.cell( i ).attribute() <= to ){
//       for ( int j = 0; j < mesh.cell( i ).nodeCount(); j ++ ){
// 	if ( nodeMap[ mesh.cell( i ).node( j ).id() ] == 0 ){
// 	  count ++;
// 	  nodeMap[ mesh.cell( i ).node( j ).id() ] = count;
// 	  this->createNode( mesh.cell( i ).node( j ).pos(), count - 1, 0 );
// 	}
//       }
//     }
//   }

//   for ( int i = 0; i < mesh.cellCount(); i ++ ){
//     if ( mesh.cell( i ).attribute() >= from && mesh.cell( i ).attribute() <= to ){
//       switch( mesh.dim() ){
//       case 2: dynamic_cast < Mesh2D *>(this)->createTriangle( this->node( nodeMap[ mesh.cell( i ).node( 0 ).id() ] - 1 ),
// 							       this->node( nodeMap[ mesh.cell( i ).node( 1 ).id() ] - 1 ),							       this->node( nodeMap[ mesh.cell( i ).node( 2 ).id() ] - 1 ), i , mesh.cell( i ).attribute() );
// 	break;
//       case 3: dynamic_cast < Mesh3D *>(this)->createTetrahedron( this->node( nodeMap[ mesh.cell( i ).node( 0 ).id() ] - 1 ),
// 				       this->node( nodeMap[ mesh.cell( i ).node( 1 ).id() ] - 1 ),
// 				       this->node( nodeMap[ mesh.cell( i ).node( 2 ).id() ] - 1 ),
// 				       this->node( nodeMap[ mesh.cell( i ).node( 3 ).id() ] - 1 ), i ,
// 				       mesh.cell( i ).attribute() );
// 	break;
//       }
//     }
//   }
}

void BaseMesh::initMat( double mat[ 4 ][ 4 ] ){
  for ( int i = 0; i < 4; i ++){
    for ( int j = 0; j < 4; j ++){
      mat[ i ][ j ] = 0.0;
    }
  }
  mat[ 0 ][ 0 ] = 1.0;
  mat[ 1 ][ 1 ] = 1.0;
  mat[ 2 ][ 2 ] = 1.0;
}

void BaseMesh::rotate( double phix, double phiy, double phiz, const RealPos & relative ){
  translate( relative * -1.0 );

  _isworldmatrixknown = 0;
  double mat[ 4 ][ 4 ];
  if ( phix != 0.0 ) {
    initMat( mat );
    mat[ 1 ][ 1 ] = cos( phix ); mat[ 1 ][ 2 ] = -1. * sin( phix );
    mat[ 2 ][ 1 ] = sin( phix ); mat[ 2 ][ 2 ] = cos( phix );
    transform( mat );
//     cout << "Matrix" << endl
// 	 << "\t" << mat[0][0] << "\t" << mat[0][1] << "\t" << mat[0][2] <<"\t"<< mat[0][3] << endl
// 	 << "\t" << mat[1][0] << "\t" << mat[1][1] << "\t" << mat[1][2] <<"\t"<< mat[1][3] << endl
// 	 << "\t" << mat[2][0] << "\t" << mat[2][1] << "\t" << mat[2][2] <<"\t"<< mat[2][3] << endl
// 	 << "\t" << mat[3][0] << "\t" << mat[3][1] << "\t" << mat[3][2] <<"\t"<< mat[3][3] << endl ;
  }
  if ( phiy != 0.0 ) {
    initMat( mat );
    mat[ 0 ][ 0 ] = cos( phiy );      mat[ 0 ][ 2 ] = sin( phiy );
    mat[ 2 ][ 0 ] = -1. * sin( phiy ); mat[ 2 ][ 2 ] = cos( phiy );
    transform( mat );
//     cout << "Matrix" << endl
// 	 << "\t" << mat[0][0] << "\t" << mat[0][1] << "\t" << mat[0][2] <<"\t"<< mat[0][3] << endl
// 	 << "\t" << mat[1][0] << "\t" << mat[1][1] << "\t" << mat[1][2] <<"\t"<< mat[1][3] << endl
// 	 << "\t" << mat[2][0] << "\t" << mat[2][1] << "\t" << mat[2][2] <<"\t"<< mat[2][3] << endl
// 	 << "\t" << mat[3][0] << "\t" << mat[3][1] << "\t" << mat[3][2] <<"\t"<< mat[3][3] << endl ;
  }
  if ( phiz != 0.0 ) {
    initMat( mat );
    mat[ 0 ][ 0 ] = cos( phiz );  mat[ 0 ][ 1 ] = -1. * sin( phiz );
    mat[ 1 ][ 0 ] = sin( phiz );  mat[ 1 ][ 1 ] = cos( phiz );
    transform( mat );
//     cout << "Matrix" << endl
// 	 << "\t" << mat[0][0] << "\t" << mat[0][1] << "\t" << mat[0][2] <<"\t"<< mat[0][3] << endl
// 	 << "\t" << mat[1][0] << "\t" << mat[1][1] << "\t" << mat[1][2] <<"\t"<< mat[1][3] << endl
// 	 << "\t" << mat[2][0] << "\t" << mat[2][1] << "\t" << mat[2][2] <<"\t"<< mat[2][3] << endl
// 	 << "\t" << mat[3][0] << "\t" << mat[3][1] << "\t" << mat[3][2] <<"\t"<< mat[3][3] << endl ;
  }
  translate( relative );
}

void BaseMesh::translate( double tx, double ty, double tz ){
  _isworldmatrixknown = 0;
  double mat[ 4 ][ 4 ];
  initMat( mat );
  mat[ 0 ][ 3 ] = tx;
  mat[ 1 ][ 3 ] = ty;
  mat[ 2 ][ 3 ] = tz;
  transform( mat );
}

void BaseMesh::scale( double sx, double sy, double sz ){
  _isworldmatrixknown = 0;
  double mat[ 4 ][ 4 ];
  initMat( mat );
  if ( sy == 0.0 && sz == 0.0 ){
    sy = sx; sz = sx;
  }
  mat[ 0 ][ 0 ] *= sx;
  mat[ 1 ][ 1 ] *= sy;
  mat[ 2 ][ 2 ] *= sz;
  transform( mat );
}

int BaseMesh::saveVTKUnstructured( const string & fbody, const RVector & data, bool logScaleData ){;
// bool binary = true;
  bool binary = false;
  fstream file; if ( ! openOutFile( fbody + ".vtk", & file ) ) { return 0; }

  file.precision( 14 );
  file << "# vtk DataFile Version 3.0" << endl;
  file << "created by " << WHERE_AM_I << endl;
  if ( binary ){
    file << "BINARY" << endl;
  } else {
    file << "ASCII" << endl;
  }
  file << "DATASET UNSTRUCTURED_GRID" << endl;
  file << "POINTS " << nodeCount() << " double" << endl;

  for ( int i = 0; i < nodeCount(); i ++ ){
    if ( binary ){
      file.write( (char*)&node( i ).pos()[0], sizeof( double ) );
      file.write( (char*)&node( i ).pos()[1], sizeof( double ) );
      file.write( (char*)&node( i ).pos()[2], sizeof( double ) );
    } else {
      file << node( i ).pos().x() << "\t"
	   << node( i ).pos().y() << "\t"
	   << node( i ).pos().z() << endl;
    }
  }
  if ( binary ){ file << endl; }

  bool cells = true;
  if ( cells && cellCount() > 0 ){

    int nCellsSize = cellCount() * ( cell( 0 ).nodeCount() + 1 );
    //   if ( boundaryCount() > 0 ){
    //     nCellsSize += boundaryCount() *( boundary( 0 ).nodeCount() + 1 );
    //   }

    file << "CELLS " << cellCount() << " " << nCellsSize << endl;
    //  file << "CELLS " << cellCount() + boundaryCount() << " " << nCellsSize << endl;

    long iDummy;
    for ( int i = 0, imax = cellCount(); i < imax; i ++){
      if ( binary ){
	iDummy = cell( i ).nodeCount();
	file.write( (char*)&iDummy, sizeof( iDummy ) );
      } else {
	file << cell( i ).nodeCount() << "\t";
      }
      if ( cell( i ).rtti() ==MYMESH_TETRAHEDRON10_RTTI ){
        file << cell( i ).node( 0 ).id() << "\t" << cell( i ).node( 1 ).id() << "\t"
            << cell( i ).node( 2 ).id() << "\t" << cell( i ).node( 3 ).id() << "\t"
            << cell( i ).node( 4 ).id() << "\t" << cell( i ).node( 7 ).id() << "\t"
            << cell( i ).node( 5 ).id() << "\t" << cell( i ).node( 6 ).id() << "\t"
            << cell( i ).node( 9 ).id() << "\t" << cell( i ).node( 8 ).id();
      } else {
        for ( int j = 0, jmax = cell( i ).nodeCount(); j < jmax; j ++){
	   if ( binary ){
	       iDummy = cell( i ).node(j).id();
	       file.write( (char*)&iDummy, sizeof( iDummy ) );
           } else {
	       file << cell( i ).node(j).id() << "\t";
	   }
        }
      }
      if ( !binary ) file << endl;
    }
    if ( binary ) file << endl;
    //   for ( int i = 0, imax = boundaryCount(); i < imax; i ++){
    //     file << boundary( i ).nodeCount() << "\t";
    //     for ( int j = 0, jmax = boundary( i ).nodeCount(); j < jmax; j ++){
    //       file << boundary( i ).node( j ).id() << "\t";
    //     }
    //     file << endl;
    //   }

    file << "CELL_TYPES " << cellCount() << endl;
    iDummy = 10;
    for ( int i = 0, imax = cellCount(); i < imax; i ++) {
      if ( binary ){
	file.write( (char*)&iDummy, sizeof( iDummy ) );
      } else {
	switch ( cell(i).rtti() ){
	case MYMESH_EDGE_RTTI: file << "3 "; break;
	case MYMESH_TRIANGLE_RTTI: file << "5 "; break;
	case MYMESH_TRIANGLE6_RTTI: file << "22 "; break;
	case MYMESH_TETRAHEDRON_RTTI: file << "10 "; break;
	case MYMESH_TETRAHEDRON10_RTTI: file << "24 "; break;
	case MYMESH_HEXAHEDRON_RTTI: file << "12 "; break;
	default: cerr << WHERE_AM_I << " nothing know about." << cell(i).rtti() << endl;
	}
      }
    }
    file << endl;

    file << "CELL_DATA " << cellCount() << endl;

    if ( data.size() == cellCount() ){
      file << "SCALARS Resistivity double 1" << endl;
      file << "LOOKUP_TABLE default" << endl;

      for ( int i = 0, imax = cellCount(); i < imax; i ++) {
	if ( binary ){
	  //	  file.write( (char*)&scaledValues[ i ], sizeof( double ) );
	} else {
	  file << data[ i ] << " ";
	}
      }
      file << endl;

      file << "SCALARS Resistivity(log10) double 1" << endl;
      file << "LOOKUP_TABLE default" << endl;
      for ( int i = 0, imax = cellCount(); i < imax; i ++) {
	if ( binary ){
	  //	  file.write( (char*)&scaledValues[ i ], sizeof( double ) );
	} else {
	  file << log10( fabs(data[ i ]) + 1e-16 ) << " ";
	}
      }
      file << endl;

      file << "SCALARS Sensitivity(logdrop) double 1" << endl;
      file << "LOOKUP_TABLE default" << endl;
      double logdrop = 1e-5;
      RVector dataTmp = absolute( data / logdrop );
      for ( int i = 0; i < dataTmp.size(); i ++ ) if ( dataTmp[ i ] < 1 ) dataTmp[ i ] = 1;

      for ( int i = 0, imax = cellCount(); i < imax; i ++) {
	if ( binary ){
	  //	  file.write( (char*)&scaledValues[ i ], sizeof( double ) );
	} else {
	  file << log10( dataTmp[ i ] ) * ( sign( data[ i ] ) ) << " ";
	}
      }
      file << endl;


      file << "SCALARS Sensitivity(logdrop_scaled) double 1" << endl;
      file << "LOOKUP_TABLE default" << endl;
      logdrop = 1e-10;
      for ( int i = 0; i < dataTmp.size(); i ++ ) {
        dataTmp[ i ] = data[ i ] / this->cell( i ).area();
      }
      double maxDataTmp = max( absolute( dataTmp ) );
      dataTmp = dataTmp /  maxDataTmp;

      for ( int i = 0; i < dataTmp.size(); i ++ ) {
        dataTmp[ i ] = fabs( dataTmp[ i ] / logdrop );
        if ( dataTmp[ i ] < 1 ) dataTmp[ i ] = 1;
      }

      dataTmp = log10( dataTmp );
      dataTmp /= max( dataTmp );

      for ( int i = 0, imax = cellCount(); i < imax; i ++) {
        if ( binary ){
          //file.write( (char*)&scaledValues[ i ], sizeof( double ) );
        } else {
          file << dataTmp[ i ] * ( sign( data[ i ] ) ) << " ";
          //file << log10( dataTmp[ i ] )  * ()<< " ";
        }
      }
      file << endl;
    }
    RVector coverage;
    coverage.load( "sensCov.vector", Ascii, false, false );
    if ( coverage.size() == cellCount() ){
      file << "SCALARS coverage(log10) double 1" << endl;
      file << "LOOKUP_TABLE default" << endl;
      for ( int i = 0, imax = cellCount(); i < imax; i ++) {
        file << log10( coverage[ i ] ) << " ";
      }
      file << endl;
    }

    RVector ipData;
    ipData.load( "ip_model.vector", Ascii, false, false );

    if ( ipData.size() == cellCount() ){
      file << "SCALARS ip double 1" << endl;
      file << "LOOKUP_TABLE default" << endl;
      for ( int i = 0, imax = cellCount(); i < imax; i ++) {
        file << ipData[ i ] << " ";
      }
      file << endl;
    }


    file << "SCALARS Material double 1" << endl;
    file << "LOOKUP_TABLE default" << endl;

    for ( int i = 0, imax = cellCount(); i < imax; i ++) {
      if ( binary ){
	//	  file.write( (char*)&scaledValues[ i ], sizeof( double ) );
      } else {
	file << cell(i).attribute() << " ";
      }
    }
    file << endl;
  } else if ( boundaryCount() > 0 ){//  if cellcount == 0 but boundary exist -> surface mesh

    int nBoundSize = boundaryCount() * ( boundary( 0 ).nodeCount() + 1 );

    file << "CELLS " << boundaryCount() << " " << nBoundSize << endl;

    long iDummy;
    for ( int i = 0, imax = boundaryCount(); i < imax; i ++){
      if ( binary ){
	iDummy = boundary( i ).nodeCount();
	file.write( (char*)&iDummy, sizeof( iDummy ) );
      } else {
	file << boundary( i ).nodeCount() << "\t";
      }
      for ( int j = 0, jmax = boundary( i ).nodeCount(); j < jmax; j ++){
	if ( binary ){
	  iDummy = boundary( i ).node(j).id();
	  file.write( (char*)&iDummy, sizeof( iDummy ) );
	} else {
	  file << boundary( i ).node(j).id() << "\t";
	}
      }
      if ( !binary ) file << endl;
    }
    if ( binary ) file << endl;

    file << "CELL_TYPES " << boundaryCount() << endl;
    iDummy = 10;
    for ( int i = 0, imax = boundaryCount(); i < imax; i ++) {
      if ( binary ){
	file.write( (char*)&iDummy, sizeof( iDummy ) );
      } else {
	switch ( boundary( i ).rtti() ){
	case MYMESH_EDGE_RTTI: file << "3 "; break;
	case MYMESH_TRIANGLEFACE_RTTI: file << "5 "; break;
// 	case MYMESH_TRIANGLE6_RTTI: file << "22 "; break;
// 	case MYMESH_TETRAHEDRON_RTTI: file << "10 "; break;
// 	case MYMESH_TETRAHEDRON10_RTTI: file << "24 "; break;
	default: cerr << WHERE_AM_I << " nothing know about." << boundary(i).rtti() << endl;
	}
      }
    }
    file << endl;
    file << "CELL_DATA " << boundaryCount() << endl;

    file << "SCALARS Material double 1" << endl;
    file << "LOOKUP_TABLE default" << endl;

    for ( int i = 0, imax = boundaryCount(); i < imax; i ++) {
      if ( binary ){
	//	  file.write( (char*)&scaledValues[ i ], sizeof( double ) );
      } else {
	file << boundary( i ).marker( ) << " ";
      }
    }
    file << endl;
  }

  if ( data.size() == nodeCount() ){

    double value = 0.0;
    double eps = epsilon( data, 1 );
    //    cout << "epsilon :"  << eps << endl;

    RVector scaledValues( absolute( data ) );
    if ( logScaleData ) {
      double logdrop = 1e-4;
      //      double logdrop = eps;

      for ( int i = 0; i < scaledValues.size(); i ++ ) {
 	scaledValues[ i ] = fabs( scaledValues[ i ] / logdrop );
 	if ( scaledValues[ i ] < 1 ) scaledValues[ i ] = 1;
      }
      scaledValues = log10( scaledValues );
      //      scaledValues /= max( scaledValues );

      //      scaledValues = absolute(log10( absolute( (data) / epsilon ) + epsilon ) )* ( data / absolute(data) ); ;
      //      scaledValues = absolute( log10( absolute( data / epsilon ) ) ) * ( data / absolute(data) );
      //      scaledValues = log10( absolute( data ) + eps ) * ( ( data + eps ) / (absolute(data) + eps ) );
      //      scaledValues = log10( absolute( data ) + 1e-5 ) ;

    }

    file << "POINT_DATA " << nodeCount() << endl;
    if ( logScaleData ) {
      file << "SCALARS Potential(log10) double 1" << endl;
    } else {
      file << "SCALARS Potential double 1" << endl;
    }
    file << "LOOKUP_TABLE default" << endl;

    for ( int i = 0, imax = nodeCount(); i < imax; i ++) {
      if ( binary ){
	file.write( (char*)&scaledValues[ i ], sizeof( double ) );
      } else {
	file << scaledValues[ i ] * sign( data[i] )<< " ";
      }
    }
    file << endl;
  }
  file.close();
  return 1;
}

int BaseMesh::saveVTKUnstructured( const string & fbody, const vector < RealPos > & vecData ){
// bool binary = true;
  bool binary = false;
  fstream file; if ( ! openOutFile( fbody + ".vtk", & file ) ) { return 0; }

  file.precision( 14 );
  file << "# vtk DataFile Version 3.0" << endl;
  file << "created by " << WHERE_AM_I << endl;
  if ( binary ){
    file << "BINARY" << endl;
  } else {
    file << "ASCII" << endl;
  }
  file << "DATASET UNSTRUCTURED_GRID" << endl;
  file << "POINTS " << nodeCount() << " double" << endl;
  for ( int i = 0; i < nodeCount(); i ++ ){
    if ( binary ){
      file.write( (char*)&node( i ).pos()[0], sizeof( double ) );
      file.write( (char*)&node( i ).pos()[1], sizeof( double ) );
      file.write( (char*)&node( i ).pos()[2], sizeof( double ) );
    } else {
      file << node( i ).pos().x() << "\t"
	   << node( i ).pos().y() << "\t"
	   << node( i ).pos().z() << endl;
    }
  }
  if ( binary ){ file << endl; }

  if ( cellCount() > 0 ){

    int nCellsSize = cellCount() * ( cell( 0 ).nodeCount() + 1 );
    //   if ( boundaryCount() > 0 ){
    //     nCellsSize += boundaryCount() *( boundary( 0 ).nodeCount() + 1 );
    //   }

    file << "CELLS " << cellCount() << " " << nCellsSize << endl;
    //  file << "CELLS " << cellCount() + boundaryCount() << " " << nCellsSize << endl;

    long iDummy;
    for ( int i = 0, imax = cellCount(); i < imax; i ++){
      if ( binary ){
	iDummy = cell( i ).nodeCount();
	file.write( (char*)&iDummy, sizeof( iDummy ) );
      } else {
	file << cell( i ).nodeCount() << "\t";
      }
      if ( cell( i ).rtti() ==MYMESH_TETRAHEDRON10_RTTI ){
      file << cell( i ).node( 0 ).id() << "\t" << cell( i ).node( 1 ).id() << "\t"
             << cell( i ).node( 2 ).id() << "\t" << cell( i ).node( 3 ).id() << "\t"
             << cell( i ).node( 4 ).id() << "\t" << cell( i ).node( 7 ).id() << "\t"
             << cell( i ).node( 5 ).id() << "\t" << cell( i ).node( 6 ).id() << "\t"
             << cell( i ).node( 9 ).id() << "\t" << cell( i ).node( 8 ).id();
      } else {
        for ( int j = 0, jmax = cell( i ).nodeCount(); j < jmax; j ++){
	 if ( binary ){
	    iDummy = cell( i ).node(j).id();
	   file.write( (char*)&iDummy, sizeof( iDummy ) );
	 } else {
	    file << cell( i ).node(j).id() << "\t";
	 }
        }
      }
      if ( !binary ) file << endl;
    }
    if ( binary ) file << endl;
    //   for ( int i = 0, imax = boundaryCount(); i < imax; i ++){
    //     file << boundary( i ).nodeCount() << "\t";
    //     for ( int j = 0, jmax = boundary( i ).nodeCount(); j < jmax; j ++){
    //       file << boundary( i ).node( j ).id() << "\t";
    //     }
    //     file << endl;
    //   }

    file << "CELL_TYPES " << cellCount() << endl;
    iDummy = 10;
    for ( int i = 0, imax = cellCount(); i < imax; i ++) {
      if ( binary ){
	file.write( (char*)&iDummy, sizeof( iDummy ) );
      } else {
	switch ( cell(i).rtti() ){
	case MYMESH_EDGE_RTTI: file << "3 "; break;
	case MYMESH_TRIANGLE_RTTI: file << "5 "; break;
	case MYMESH_TRIANGLE6_RTTI: file << "22 "; break;
	case MYMESH_TETRAHEDRON_RTTI: file << "10 "; break;
	case MYMESH_TETRAHEDRON10_RTTI: file << "24 "; break;
	default: cerr << WHERE_AM_I << " nothing know about." << cell(i).rtti() << endl;
	}
      }
    }
    file << endl;

    if ( vecData.size() == (size_t)cellCount() ){
      file << "CELL_DATA " << cellCount() << endl;
      file << "VECTORS gradU double" << endl;
      for ( int i = 0, imax = cellCount(); i < imax; i ++) {
	if ( binary ){
	  //	  file.write( (char*)&scaledValues[ i ], sizeof( double ) );
	} else {
	  file << vecData[ i ][ 0 ] << " " << vecData[ i ][ 1 ]  << " " << vecData[ i ][ 2 ] << endl;
	}
      }
      file << endl;
    }
  } else if ( boundaryCount() > 0 ){//  if cellcount == 0 but boundary exist -> surface mesh

    int nBoundSize = boundaryCount() * ( boundary( 0 ).nodeCount() + 1 );

    file << "CELLS " << boundaryCount() << " " << nBoundSize << endl;

    long iDummy;
    for ( int i = 0, imax = boundaryCount(); i < imax; i ++){
      if ( binary ){
	iDummy = boundary( i ).nodeCount();
	file.write( (char*)&iDummy, sizeof( iDummy ) );
      } else {
	file << boundary( i ).nodeCount() << "\t";
      }
      for ( int j = 0, jmax = boundary( i ).nodeCount(); j < jmax; j ++){
	if ( binary ){
	  iDummy = boundary( i ).node(j).id();
	  file.write( (char*)&iDummy, sizeof( iDummy ) );
	} else {
	  file << boundary( i ).node(j).id() << "\t";
	}
      }
      if ( !binary ) file << endl;
    }
    if ( binary ) file << endl;

    file << "CELL_TYPES " << boundaryCount() << endl;
    iDummy = 10;
    for ( int i = 0, imax = boundaryCount(); i < imax; i ++) {
      if ( binary ){
	file.write( (char*)&iDummy, sizeof( iDummy ) );
      } else {
	switch ( boundary( i ).rtti() ){
	case MYMESH_EDGE_RTTI: file << "3 "; break;
	case MYMESH_TRIANGLEFACE_RTTI: file << "5 "; break;
// 	case MYMESH_TRIANGLE6_RTTI: file << "22 "; break;
// 	case MYMESH_TETRAHEDRON_RTTI: file << "10 "; break;
// 	case MYMESH_TETRAHEDRON10_RTTI: file << "24 "; break;
	default: cerr << WHERE_AM_I << " nothing know about." << boundary(i).rtti() << endl;
	}
      }
    }
    file << endl;
    file << "CELL_DATA " << boundaryCount() << endl;

    file << "SCALARS Material double 1" << endl;
    file << "LOOKUP_TABLE default" << endl;

    for ( int i = 0, imax = boundaryCount(); i < imax; i ++) {
      if ( binary ){
	//	  file.write( (char*)&scaledValues[ i ], sizeof( double ) );
      } else {
	file << boundary( i ).marker( ) << " ";
      }
    }
    file << endl;
  }

  if ( vecData.size() == (size_t)nodeCount() ){

    file << "POINT_DATA " << nodeCount() << endl;
    file << "VECTORS gradU double" << endl;
    for ( int i = 0, imax = nodeCount(); i < imax; i ++) {
      if ( binary ){
	//	file.write( (char*)&vecData[ i ], sizeof( double ) );
      } else {
	file << vecData[ i ][ 0 ] << " " << vecData[ i ][ 1 ] << " " << vecData[ i ][ 2 ]<< endl;
      }
    }
    file << endl;
  }
  file.close();
  return 1;
}

int BaseMesh::loadGrummpMesh( const string & fbody ){
  clear();
//   fstream file; if ( ! openInFile( fbody+".mesh", & file ) ) { return 0; }

//   int ncells = 0, nfaces = 0, nbfaces = 0, nverts = 0;
//   int marker = 0;
//   double x, y;

//   file >> ncells >> nfaces >> nbfaces >> nverts;

//   for ( int i = 0, imax = nverts; i < imax; i++ ){
//     file >> x >> y;
//     createNode( x, y, i, marker );
//   }

//   int a = 0, b = 0, c = 0;

//   for ( int i = 0, imax = nfaces; i < imax; i++ ){
//     file >> a >> b;
//     createEdge( node(a), node(b), i, marker );
//   }

//   for ( int i = 0, imax = ncells; i < imax; i++ ){
//     file >> a >> b >> c >> marker;
//     createTriangle( node(a), node(b), node(c), i, marker );
//   }

//   for ( int i = 0, imax = nbfaces; i < imax; i++ ){
//     file >> a  >> marker;
//     edge(a).setMarker(marker);
//     edge(a).nodeA().setMarker(marker);
//     edge(a).nodeB().setMarker(marker);
//   }

//   file.close();

  return -1;
}

} // namespace My2DMesh

/*
$Log: basemesh.cpp,v $
Revision 1.77  2011/08/30 14:09:51  carsten
fix memory problem within mesh-copy

Revision 1.76  2011/05/10 14:37:35  carsten
small configure patch

Revision 1.75  2010/10/26 12:20:28  carsten
win32 compatibility commit

Revision 1.74  2009/01/30 16:54:26  carsten
*** empty log message ***

Revision 1.73  2008/12/11 15:14:13  carsten
*** empty log message ***

Revision 1.72  2008/10/05 13:25:00  carsten
*** empty log message ***

Revision 1.71  2008/05/15 09:15:22  thomas
no message

Revision 1.70  2008/04/15 16:53:04  carsten
*** empty log message ***

Revision 1.69  2008/02/24 21:58:52  carsten
*** empty log message ***

Revision 1.68  2007/12/04 20:10:50  carsten
*** empty log message ***

Revision 1.67  2007/11/16 16:23:17  carsten
*** empty log message ***

Revision 1.65  2007/10/01 17:00:36  carsten
*** empty log message ***

Revision 1.64  2007/09/23 21:32:40  carsten
*** empty log message ***

Revision 1.63  2007/03/22 20:29:02  carsten
*** empty log message ***

Revision 1.62  2007/03/15 18:04:14  carsten
*** empty log message ***

Revision 1.61  2007/03/15 16:14:01  carsten
*** empty log message ***

Revision 1.60  2007/02/21 19:44:05  carsten
*** empty log message ***

Revision 1.59  2007/02/12 13:46:40  carsten
*** empty log message ***

Revision 1.58  2007/01/25 17:38:13  carsten
*** empty log message ***

Revision 1.57  2007/01/22 16:03:22  carsten
*** empty log message ***

Revision 1.56  2007/01/08 21:42:19  carsten
*** empty log message ***

Revision 1.55  2006/12/28 21:55:29  carsten
*** empty log message ***

Revision 1.54  2006/10/08 14:11:38  carsten
*** empty log message ***

Revision 1.53  2006/09/21 17:08:40  carsten
*** empty log message ***

Revision 1.52  2006/07/19 19:20:18  carsten
*** empty log message ***

Revision 1.51  2006/05/08 17:43:16  carsten
*** empty log message ***

Revision 1.50  2006/02/06 17:30:42  carsten
*** empty log message ***

Revision 1.49  2005/12/06 13:57:26  carsten
*** empty log message ***

Revision 1.46  2005/10/28 14:35:55  carsten
*** empty log message ***

Revision 1.45  2005/10/24 09:52:08  carsten
*** empty log message ***

Revision 1.44  2005/10/20 11:25:05  carsten
*** empty log message ***

Revision 1.43  2005/10/18 16:29:56  carsten
*** empty log message ***

Revision 1.42  2005/10/17 11:56:58  carsten
*** empty log message ***

Revision 1.41  2005/10/14 15:31:37  carsten
*** empty log message ***

Revision 1.40  2005/10/12 16:37:43  carsten
*** empty log message ***

Revision 1.39  2005/10/12 15:32:07  carsten
*** empty log message ***

Revision 1.37  2005/10/09 18:54:44  carsten
*** empty log message ***

Revision 1.36  2005/10/04 18:05:51  carsten
*** empty log message ***

Revision 1.35  2005/10/04 11:07:42  carsten
*** empty log message ***

Revision 1.34  2005/09/26 11:17:31  carsten
*** empty log message ***

Revision 1.33  2005/08/22 17:28:09  carsten
*** empty log message ***

Revision 1.32  2005/08/12 16:43:20  carsten
*** empty log message ***

Revision 1.31  2005/08/09 18:13:44  carsten
*** empty log message ***

Revision 1.30  2005/07/28 19:43:43  carsten
*** empty log message ***

Revision 1.29  2005/07/20 14:04:48  carsten
*** empty log message ***

Revision 1.28  2005/07/14 15:03:37  carsten
*** empty log message ***

Revision 1.27  2005/06/28 16:38:15  carsten
*** empty log message ***

Revision 1.26  2005/06/13 10:21:01  carsten
*** empty log message ***

Revision 1.25  2005/06/09 11:21:08  carsten
*** empty log message ***

Revision 1.24  2005/06/02 15:44:10  carsten
*** empty log message ***

Revision 1.23  2005/06/02 10:50:45  carsten
*** empty log message ***

Revision 1.21  2005/06/01 18:26:51  carsten
*** empty log message ***

Revision 1.20  2005/04/11 13:31:23  carsten
*** empty log message ***

Revision 1.19  2005/04/07 11:41:08  carsten
*** empty log message ***

Revision 1.18  2005/03/18 19:47:17  carsten
*** empty log message ***

Revision 1.17  2005/03/10 16:04:38  carsten
*** empty log message ***

Revision 1.16  2005/03/09 19:20:47  carsten
*** empty log message ***

Revision 1.15  2005/03/08 16:04:45  carsten
*** empty log message ***

Revision 1.14  2005/02/18 17:47:33  carsten
*** empty log message ***

Revision 1.13  2005/02/18 14:06:19  carsten
*** empty log message ***

Revision 1.12  2005/02/16 19:36:38  carsten
*** empty log message ***

Revision 1.11  2005/02/14 19:37:32  carsten
*** empty log message ***

Revision 1.10  2005/02/10 14:07:59  carsten
*** empty log message ***

Revision 1.8  2005/01/13 20:10:42  carsten
*** empty log message ***

Revision 1.7  2005/01/12 20:32:40  carsten
*** empty log message ***

Revision 1.6  2005/01/07 15:23:05  carsten
*** empty log message ***

Revision 1.5  2005/01/06 19:46:00  carsten
*** empty log message ***

Revision 1.3  2005/01/06 16:15:56  carsten
*** empty log message ***

Revision 1.2  2005/01/04 19:44:05  carsten
*** empty log message ***

Revision 1.1  2005/01/04 19:22:30  carsten
add in cvs

*/


